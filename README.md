# POLAR WP 1.6 Pilot

WP 1.6 pilot container registry project for the POLAR MII Use Case. This project solely exists for the private GitLab Container Registry and will be removed in the future, when the images become public or are moved to a dedicated location.
